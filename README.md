We define tasks based on QWorld’s introductory tutorial called Bronze (https://qworld.net/workshop-bronze/).


There are three version in the following branches: 

1. **qsd2021** -presented at QWorld Quantum Science Days 2021 (https://qworld.net/qscience-days/)  

2. **TQC2021** -presented at TQC 2021 16th Conference on the Theory of Quantum Computation, Communication and Cryptography (https://tqc2021.lu.lv/)  

3. **CAI2021** -presented at KKIO 2021 Software Engineering Conference (https://kkio.pti.org.pl/2021/) 

3. **BJMC** -created for Baltic Journal of Modern Computing (https://www.bjmc.lu.lv/)